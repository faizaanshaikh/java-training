package com.digitaslbi.training;

final class Calculator2 {
	
	int a;
	int b;
	int result;
	static int staticResult;

	public Calculator2(int p, int q) {
		this.a = p;
		this.b = q;
	}
	
	
	void add() {
		
		
		
		this.result = this.a + this.b;
		staticResult = staticResult + this.result;
	}

	void print() {
		System.out.println(this.a +" + "+ this.b + " = "+ this.result+ "This has hashcode "+this.hashCode());
		System.out.println("Static Result: "+Calculator2.staticResult);
	}
	
}
