package com.digitaslbi.training;

public class Car {
	
	String engine;
	String colour;

	public Car(String engine, String colour) {
		this.engine = engine;
		this.colour = colour;
	}
	
	@Override
	public String toString() {
		return engine + " "+ colour;
	}
	
}
