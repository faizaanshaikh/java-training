package com.digitaslbi.training;

import java.util.Date;

import com.digitaslbi.training.helper.AutomationHelper;

public class Automation  extends Object{

	public Automation() {
		
	

	}

	public static void doAutomation() {
		AutomationHelper automationHelper = new AutomationHelper();
		automationHelper.automateTestCase1();
		automationHelper.automateTestCase2();
		
		
	}

	public static String getTime() {
		return new Date().toString();
	}

	public static void main(String[] args) {
		
		

		System.out.println("Started Automation at " + getTime());
		doAutomation();
		System.out.println("Completed Automation at " + getTime());
	}

}
