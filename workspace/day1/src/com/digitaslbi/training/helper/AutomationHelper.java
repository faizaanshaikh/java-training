package com.digitaslbi.training.helper;

public class AutomationHelper {

	public AutomationHelper() {

	}

	public void automateTestCase1() {
		System.out.println("Automating test case 1");
		this.addWait();

	}

	public void automateTestCase2() {
		System.out.println("Automating test case 2");
		this.addWait();
	}

	public void addWait() {
		try {
			Thread.currentThread().sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
