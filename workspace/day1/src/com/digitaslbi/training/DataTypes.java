package com.digitaslbi.training;

public class DataTypes {
	
	//Primary
	int intVar;
	float floatVar;
	double doubleVar;
	long longVar;
	
	//Non primary or Reference
	String stringVar;
	Integer intBoxVar;
	Double doubleBoxVar;
	Long longBoxVar;
	

	public DataTypes() {
	
		intVar = Integer.parseInt("3");
		intBoxVar = Integer.valueOf(intVar);
		intBoxVar = Integer.valueOf("3");
		
		Integer i = new Integer(4);
		intVar = i;
		
		
	}

}
