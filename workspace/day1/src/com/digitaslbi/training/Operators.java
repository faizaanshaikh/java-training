package com.digitaslbi.training;

public class Operators {

	public Operators() {
		
	}

	public static void main(String[] args) {
		
		// + - / *  
		//&& || ! !=
		//> < >= <=
		
		int i = 10;
		int j = 15;
		
		i+=j; //(i=i+j)
				
		System.out.println(i);
		
		int p = 10;
		int m = p++;
		System.out.println(m);
		
		int q = 10;
		int k = ++q;
		System.out.println(k);
		
		//Ternary OPerator (condition) ? (truth) : false;
		int a =100;
		int b = 10;
		
		String result = a > b ? "a>b" : "b<a";
		System.out.println("Result: "+result);
		
		String nestedResult = a<b ? "a>b" : (a+b > 200 ? "greater than 200" : "smaller than 200");
		
		System.out.println("Nested Result: "+nestedResult);
		
	}
	
}
