package com.digitaslbi.training;

import java.util.Date;

public class StructureDemo {
	
	//Instance Variables
	int intVariable;
	float floatVariable;
	double doubaleVariable;
	long longVariable;
	boolean booleanVariable;
	String stringVariable;

	public StructureDemo() {

	}

	public StructureDemo(int intVariable) {
		this.intVariable = intVariable;
	}

	private static void printDate() {
		Date currentDate = new Date();
		System.out.println("Hello there, current date is " + currentDate.toString());
	}

	private void printVariable() {

		System.out.println("variable: " + intVariable);
	}

	public static void main(String[] args) {

		StructureDemo.printDate();

		StructureDemo instance1 = new StructureDemo();
		instance1.intVariable = 10;
		instance1.printVariable();
		
		StructureDemo instance2 = new StructureDemo(5);
		instance2.printVariable();

	}

}
